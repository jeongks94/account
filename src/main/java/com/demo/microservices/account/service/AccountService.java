package com.demo.microservices.account.service;


import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.demo.microservices.account.dao.CommonDao;
import com.demo.microservices.account.model.Account;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class AccountService {

	@Autowired
	private CommonDao commonDao;
	
	public List<Account> getAccountAll() {
    	try {
            List<Account> accounts =  commonDao.selectList("selectAccAll");
            
            return accounts;
    	} catch (Exception e) {
    		throw new RuntimeException(e);
    	}
	}
	
	public Account getAccount(String accountNo) {
    	try {
            Account account =  commonDao.selectOne("findByAccNo", accountNo);
            
            return account;
    	} catch (Exception e) {
    		throw new RuntimeException(e);
    	}
	}
	
	public int addAccount(Account account) {
    	int rc = 0;
    	try {
            rc = commonDao.insert("addAccount", account);
            
            return rc;
    	} catch (Exception e) {
    		log.error("[ERROR]", e);
    		
    		throw new RuntimeException(e);
    	}
	}
	
	public int deleteAccount(String accountNo) {
    	int rc = 0;
    	try {
            rc = commonDao.insert("deleteAccount", accountNo);
            
            return rc;
    	} catch (Exception e) {
    		log.error("[ERROR]", e);
    		
    		throw new RuntimeException(e);
    	}
	}
	
	public int updateAccount(Account account) {
    	int rc = 0;
    	try {
            rc = commonDao.update("updateAccount", account);
            
            return rc;
    	} catch (Exception e) {
    		log.error("[ERROR]", e);
    		
    		throw new RuntimeException(e);
    	}
	}
}
